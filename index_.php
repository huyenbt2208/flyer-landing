<?php include "templates/head.php"; ?>
<body>
	<?php include "templates/header.php"; ?>
	<div class="con-featured">
		<div class="box-featured">
			<img src="images/img-featured.png" alt="">
		</div>
		
	</div>
	<div class="main-top">
		
		<?php include "templates/home/benefit.php"; ?>
		<?php include "templates/home/planet.php"; ?>

		<?php include "templates/home/comment.php"; ?>
	</div>
	
	<div class="main-bottom">
		<?php include "templates/home/course.php"; ?>

		<?php include "templates/home/news.php"; ?>

		<?php include "templates/home/partner.php"; ?>

		<?php include "templates/home/faqs.php"; ?>


		<?php include "templates/footer.php"; ?>
	</div>

	

	<script>
		$(function() {
			// ======================> benefit <======================
			$(function() {
				var benafitSlider= $('.con-benefit .box-slider').slick({
					speed: 600,
					autoplaySpeed: 6000,
					autoplay: true,
					infinite: true,
					swipe: true,
					fade: false,
					dots: false,
					arrows: false,
					slidesToShow: 3,
					slidesToScroll: 1,
					responsive: [
						{
							breakpoint: 992,
							settings: {
								slidesToShow: 2
							}
						},
						{
							breakpoint: 768,
							settings: {
								slidesToShow: 1
							}
						}
					]
				});

				$('.con-benefit .slick-prev').on('click', function() {
					benafitSlider.slick('prev');
				});
				$('.con-benefit .slick-next').on('click', function() {
					benafitSlider.slick('next');
				});
			});

			// ======================> comment <======================
			$(function() {
				var commentSlider= $('.con-comment .box-slider').slick({
					speed: 600,
					autoplaySpeed: 6000,
					autoplay: false,//
					infinite: true,
					swipe: true,
					fade: false,
					dots: false,
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1,
				});

				$('.con-comment .slick-prev').on('click', function() {
					commentSlider.slick('prev');
				});
				$('.con-comment .slick-next').on('click', function() {
					commentSlider.slick('next');
				});
			});

			// ======================> countdown <======================
			$('#countdown').timeTo({
				seconds: 2 * 60 * 60,
				fontSize: 40,
				captionSize: 14,
				displayCaptions: true,
				lang: 'vi'
			});

			// ======================> comment <======================
			$('.box-banner .list-banner').slick({
				speed: 600,
				autoplaySpeed: 6000,
				autoplay: true,
				infinite: true,
				swipe: true,
				fade: false,
				dots: false,
				arrows: false,
				slidesToShow: 5,
				centerMode: true,
				centerPadding: '30px',
					slidesToScroll: 1,
					// variableWidth: true,
					// centerMode: true,
				responsive: [
					{
						breakpoint: 992,
						settings: {
							slidesToShow: 2,
							arrows: true,
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
							arrows: false,
							slidesToScroll: 1,
						}
					}
				]
			});
		});

	</script>

</body>
</html>

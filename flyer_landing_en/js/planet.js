var positions = [
    { name: 'Position 0', value: { x: 0, y: 0 } },
    { name: 'Position 1', value: { x: 270, y: -75 } },
    { name: 'Position 2', value: { x: 500, y: -230 } },
    { name: 'Position 3', value: { x: 500, y: -540 } },
    { name: 'Position 4', value: { x: 270, y: -630 } },
    { name: 'Position 5', value: { x: 0, y: -745 } },
    { name: 'Position 6', value: { x: -270, y: -630 } },
    { name: 'Position 7', value: { x: -500, y: -540 } },
    { name: 'Position 8', value: { x: -500, y: -230 } },
    { name: 'Position 9', value: { x: -270, y: -75 } },
  ];
  var positionTotal = positions.length;
  var currentIndex = 0;
  
  function getArcArray(list, current, step) {
    if (!(Array.isArray(list) && list.length) || step === 0) return;
  
    var newList = [];
    var total = list.length;
    var start = 0;
    var end = 0;
  
    if (step > 0) {
      start = current + 1;
      end = current + 1 + step;
  
      newList = [...list].slice(start, end > total ? total : end);
  
      if (end > total) {
        var addedArray = [...list].slice(0, end - total);
  
        newList = [...newList, ...addedArray];
      }
    } else if (step < 0) {
      start = current + step;
      end = current;
  
      newList = [...list].slice(start < 0 ? 0 : start, end);
  
      if (start < 0) {
        var addedArray = [...list].slice(total + (current + step), total);
  
        newList = [...addedArray, ...newList];
      }
  
      newList = [...newList].reverse();
    }
  
    // console.log("newList", newList);
  
    return newList
  }
  
  function activePlanet0() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[0].value, 0)
      .to('[data-index="1"]', 1, newPositions[1].value, 0)
      .to('[data-index="2"]', 1, newPositions[2].value, 0)
      .to('[data-index="3"]', 1, newPositions[3].value, 0)
      .to('[data-index="4"]', 1, newPositions[4].value, 0)
      .to('[data-index="5"]', 1, newPositions[5].value, 0)
      .to('[data-index="6"]', 1, newPositions[6].value, 0)
      .to('[data-index="7"]', 1, newPositions[7].value, 0)
      .to('[data-index="8"]', 1, newPositions[8].value, 0)
      .to('[data-index="9"]', 1, newPositions[9].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  
  function activePlanet1() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[9].value, 0)
      .to('[data-index="1"]', 1, newPositions[0].value, 0)
      .to('[data-index="2"]', 1, newPositions[1].value, 0)
      .to('[data-index="3"]', 1, newPositions[2].value, 0)
      .to('[data-index="4"]', 1, newPositions[3].value, 0)
      .to('[data-index="5"]', 1, newPositions[4].value, 0)
      .to('[data-index="6"]', 1, newPositions[5].value, 0)
      .to('[data-index="7"]', 1, newPositions[6].value, 0)
      .to('[data-index="8"]', 1, newPositions[7].value, 0)
      .to('[data-index="9"]', 1, newPositions[8].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  
  function activePlanet2() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[8].value, 0)
      .to('[data-index="1"]', 1, newPositions[9].value, 0)
      .to('[data-index="2"]', 1, newPositions[0].value, 0)
      .to('[data-index="3"]', 1, newPositions[1].value, 0)
      .to('[data-index="4"]', 1, newPositions[2].value, 0)
      .to('[data-index="5"]', 1, newPositions[3].value, 0)
      .to('[data-index="6"]', 1, newPositions[4].value, 0)
      .to('[data-index="7"]', 1, newPositions[5].value, 0)
      .to('[data-index="8"]', 1, newPositions[6].value, 0)
      .to('[data-index="9"]', 1, newPositions[7].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  
  function activePlanet3() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[7].value, 0)
      .to('[data-index="1"]', 1, newPositions[8].value, 0)
      .to('[data-index="2"]', 1, newPositions[9].value, 0)
      .to('[data-index="3"]', 1, newPositions[0].value, 0)
      .to('[data-index="4"]', 1, newPositions[1].value, 0)
      .to('[data-index="5"]', 1, newPositions[2].value, 0)
      .to('[data-index="6"]', 1, newPositions[3].value, 0)
      .to('[data-index="7"]', 1, newPositions[4].value, 0)
      .to('[data-index="8"]', 1, newPositions[5].value, 0)
      .to('[data-index="9"]', 1, newPositions[6].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  function activePlanet4() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[6].value, 0)
      .to('[data-index="1"]', 1, newPositions[7].value, 0)
      .to('[data-index="2"]', 1, newPositions[8].value, 0)
      .to('[data-index="3"]', 1, newPositions[9].value, 0)
      .to('[data-index="4"]', 1, newPositions[0].value, 0)
      .to('[data-index="5"]', 1, newPositions[1].value, 0)
      .to('[data-index="6"]', 1, newPositions[2].value, 0)
      .to('[data-index="7"]', 1, newPositions[3].value, 0)
      .to('[data-index="8"]', 1, newPositions[4].value, 0)
      .to('[data-index="9"]', 1, newPositions[5].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  function activePlanet5() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[5].value, 0)
      .to('[data-index="1"]', 1, newPositions[6].value, 0)
      .to('[data-index="2"]', 1, newPositions[7].value, 0)
      .to('[data-index="3"]', 1, newPositions[8].value, 0)
      .to('[data-index="4"]', 1, newPositions[9].value, 0)
      .to('[data-index="5"]', 1, newPositions[0].value, 0)
      .to('[data-index="6"]', 1, newPositions[1].value, 0)
      .to('[data-index="7"]', 1, newPositions[2].value, 0)
      .to('[data-index="8"]', 1, newPositions[3].value, 0)
      .to('[data-index="9"]', 1, newPositions[4].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  function activePlanet6() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[4].value, 0)
      .to('[data-index="1"]', 1, newPositions[5].value, 0)
      .to('[data-index="2"]', 1, newPositions[6].value, 0)
      .to('[data-index="3"]', 1, newPositions[7].value, 0)
      .to('[data-index="4"]', 1, newPositions[8].value, 0)
      .to('[data-index="5"]', 1, newPositions[9].value, 0)
      .to('[data-index="6"]', 1, newPositions[0].value, 0)
      .to('[data-index="7"]', 1, newPositions[1].value, 0)
      .to('[data-index="8"]', 1, newPositions[2].value, 0)
      .to('[data-index="9"]', 1, newPositions[3].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  function activePlanet7() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[3].value, 0)
      .to('[data-index="1"]', 1, newPositions[4].value, 0)
      .to('[data-index="2"]', 1, newPositions[5].value, 0)
      .to('[data-index="3"]', 1, newPositions[6].value, 0)
      .to('[data-index="4"]', 1, newPositions[7].value, 0)
      .to('[data-index="5"]', 1, newPositions[8].value, 0)
      .to('[data-index="6"]', 1, newPositions[9].value, 0)
      .to('[data-index="7"]', 1, newPositions[0].value, 0)
      .to('[data-index="8"]', 1, newPositions[1].value, 0)
      .to('[data-index="9"]', 1, newPositions[2].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  function activePlanet8() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[2].value, 0)
      .to('[data-index="1"]', 1, newPositions[3].value, 0)
      .to('[data-index="2"]', 1, newPositions[4].value, 0)
      .to('[data-index="3"]', 1, newPositions[5].value, 0)
      .to('[data-index="4"]', 1, newPositions[6].value, 0)
      .to('[data-index="5"]', 1, newPositions[7].value, 0)
      .to('[data-index="6"]', 1, newPositions[8].value, 0)
      .to('[data-index="7"]', 1, newPositions[9].value, 0)
      .to('[data-index="8"]', 1, newPositions[0].value, 0)
      .to('[data-index="9"]', 1, newPositions[1].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }
  function activePlanet9() {
    var newPositions = JSON.parse(JSON.stringify(positions));
  
    new TimelineMax()
      .to('[data-index="0"]', 1, newPositions[1].value, 0)
      .to('[data-index="1"]', 1, newPositions[2].value, 0)
      .to('[data-index="2"]', 1, newPositions[3].value, 0)
      .to('[data-index="3"]', 1, newPositions[4].value, 0)
      .to('[data-index="4"]', 1, newPositions[5].value, 0)
      .to('[data-index="5"]', 1, newPositions[6].value, 0)
      .to('[data-index="6"]', 1, newPositions[7].value, 0)
      .to('[data-index="7"]', 1, newPositions[8].value, 0)
      .to('[data-index="8"]', 1, newPositions[9].value, 0)
      .to('[data-index="9"]', 1, newPositions[0].value, 0);
  
    currentIndex = 0;
  
    // $('[data-index="0"]').addClass('active');
  }

 $(".planet").click(function() {
    $('.box-planet-wrap').attr("planet-center", this.id);
    if (this.id == "planet0") activePlanet0();
    if (this.id == "planet1") activePlanet1();
    if (this.id == "planet2") activePlanet2();
    if (this.id == "planet3") activePlanet3();
    if (this.id == "planet4") activePlanet4();
    if (this.id == "planet5") activePlanet5();
    if (this.id == "planet6") activePlanet6();
    if (this.id == "planet7") activePlanet7();
    if (this.id == "planet8") activePlanet8();
    if (this.id == "planet9") activePlanet9();
 
 });
  $(function () {
    activePlanet0();
  
    $(document.body).on('click', '.planet', function () {
      var _this = $(this);
      var index = +$(this).data("index");
  
      $(".planet").removeClass("active");
  
  
  
      setTimeout(function () {
        _this.addClass("active");
      }, 1000);
    });
  
    $(".reset").on("click", function () {
      activePlanet0(0);
      // setDataIndexToElements();
    });
  });

    $(".box-planet-wrap .planet a").on("click", function () {
        var delay = 1500; //Your delay in milliseconds
        var href = $(this).attr("scrhref");
        setTimeout(function(){ 
            window.open(
                href,
                '_blank' // <- This is what makes it open in a new window.
              );
        }, delay);
    });
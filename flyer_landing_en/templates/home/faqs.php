<div class="con-faqs">
	<div class="container">
		<div class="box-faqs">
			<h3 class="title-lg"><span class="lg">Các câu hỏi thường gặp?</span></h3>

			<div class="accordion" id="accordionExample">
				<div class="accordion-item">
					<h2 class="accordion-header" id="heading01">
						<button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse01" aria-expanded="true" aria-controls="heading01">
							1. Tài khoản FLYER có miễn phí không?
 
						</button>
					</h2>
					<div id="collapse01" class="accordion-collapse collapse show" aria-labelledby="heading01" data-bs-parent="#accordionExample">
						<div class="accordion-body">
							Tất cả cấp độ trên Phòng Luyện Thi Ảo FLYER đều có ít nhất 1 đề thi thử miễn phí. Chỉ cần đăng ký tài khoản trên trang https://exam.flyer.vn/ là phụ huynh có thể truy cập vào đề thi và cho con vào thi thử. Sau khi con hoàn thành thử thách đầu tiên, phụ huynh có thể mua tài khoản trả phí để con truy cập được vào toàn bộ kho đề thi.

						</div>
					</div>
				</div>
				<div class="accordion-item">
					<h2 class="accordion-header" id="heading02">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse02" aria-expanded="false" aria-controls="collapse02">
							2. Mua tài khoản FLYER có truy cập được hết các cấp độ không?
						</button>
					</h2>
					<div id="collapse02" class="accordion-collapse collapse" aria-labelledby="heading02" data-bs-parent="#accordionExample">
						<div class="accordion-body">
							Khi đăng ký mua tài khoản thành công, con sẽ được tham gia luyện tất cả các cấp độ. Hiện các level phòng thi gồm có các bài thi của Cambridge (Starters, Movers, Flyers, KET, PET) và TOEFL (TOEFL Primary, TOEFL Junior) và bộ đề luyện thi IOE các khối lớp. Ngoài ra, con sẽ được toàn quyền truy cập các đề được cập nhật mới trong tương lai trong thời hạn sử dụng.

						</div>
					</div>
				</div>
				<div class="accordion-item">
					<h2 class="accordion-header" id="heading03">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse03" aria-expanded="false" aria-controls="collapseThree">
							3. Các bài luyện thi được biên soạn thế nào?
						</button>
					</h2>
					<div id="collapse03" class="accordion-collapse collapse" aria-labelledby="heading03" data-bs-parent="#accordionExample">
						<div class="accordion-body">
							Các bài thi đều được đội ngũ giảng viên Flyer biên soạn và tổng hợp từ các nguồn uy tín của Cambridge, ETS. Tiếp theo đề thi được gửi sang Anh và Mỹ để kiểm tra lần cuối, và ghi âm tại đó rồi gửi về Việt Nam để tải lên phòng luyện thi.

						</div>
					</div>
				</div>
				<div class="accordion-item">
					<h2 class="accordion-header" id="heading04">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse04" aria-expanded="false" aria-controls="collapse04">
							4. Số lượng đề trên FLYER là bao nhiêu? Có cập nhật thêm không? 
						</button>
					</h2>
					<div id="collapse04" class="accordion-collapse collapse" aria-labelledby="heading04" data-bs-parent="#accordionExample">
						<div class="accordion-body">
							Tổng số lượng đề thi trên Phòng Luyện Thi Ảo hiện nay là hơn 350 đề và vẫn được lên mới liên tục hàng tuần. 
						</div>
					</div>
				</div>
				<div class="accordion-item">
					<h2 class="accordion-header" id="heading05">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse05" aria-expanded="false" aria-controls="collapse05">
							5. Tài khoản FLYER dùng được trên mấy thiết bị? Có dùng trên điện thoại không?
						</button>
					</h2>
					<div id="collapse05" class="accordion-collapse collapse" aria-labelledby="heading05" data-bs-parent="#accordionExample">
						<div class="accordion-body">
							Tài khoản FLYER chỉ có thể đăng nhập trên 1 thiết bị tại 1 thời điểm. Phụ huynh hoàn toàn có thể đăng nhập sang thiết bị mới cho con luyện thi, tuy nhiên tài khoản thiết bị cũ sẽ bị đăng xuất. Khi các con đang làm bài mà có đăng nhập từ thiết bị khác, tài khoản của con sẽ bị thoát ra khỏi trang và bài con đang làm sẽ không được lưu. Vì vậy, phụ huynh cần bảo mật thông tin tài khoản cho các con.

						</div>
					</div>
				</div>
				<div class="accordion-item">
					<h2 class="accordion-header" id="heading06">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse06" aria-expanded="false" aria-controls="collapse06">
							6. Cuộc thi Phá Đảo là gì?
						</button>
					</h2>
					<div id="collapse06" class="accordion-collapse collapse" aria-labelledby="heading06" data-bs-parent="#accordionExample">
						<div class="accordion-body">
							“Phá đảo phòng thi ảo” là cuộc thi do Phòng luyện thi ảo FLYER tổ chức, lấy cốt truyện một chuyến du hành không gian, mà ở đó mỗi thí sinh nhí sẽ là một nhà du hành trong vũ trụ tiếng Anh Cambridge. Đây là sân chơi cho tất cả các bạn học sinh tiểu học, giúp thúc đẩy tinh thần thi đua lành mạnh của các con và trau dồi kiến thức tiếng Anh bằng niềm vui và sự yêu thích khám phá. Cuộc thi sẽ diễn ra miễn phí từ ngày 23 đến 30 hàng tháng.
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- /.con-faqs -->

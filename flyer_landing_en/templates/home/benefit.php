<div class="con-benefit">
	<div class="container">
		<h3 class="title-lg">Con được gì khi luyện thi cùng Flyer</h3>

		<div class="box-slider-wrapper">
			<div class="box-slider">
				<div class="slide-item">
					<img src="images/img-featured1.png" alt="Đề thi đa dạng, phong phú và sát với thực tế" class="img">
					<div class="txt-title">Đề thi đa dạng, phong phú và sát với thực tế</div>
					<div class="txt">Hơn 250  đề thi xây dựng mô phỏng bài  thi Cambridge, TOEFL, IOE liên tục được cập nhật.</div>
				</div>

				<div class="slide-item">
					<img src="images/img-featured2.png" alt="Đồ họa sinh động phù hợp lứa tuổi" class="img">
					<div class="txt-title">Đồ họa sinh động phù hợp lứa tuổi</div>
					<div class="txt">Hình ảnh được thiết kế bởi các họa sỹ và chuyên gia hoạt hình giàu kinh nghiệm, phù hợp với mọi lứa tuổi</div>
				</div>

				<div class="slide-item">
					<img src="images/img-featured3.png" alt="Ứng dụng mô phỏng trò chơi đầy  thú vị" class="img">
					<div class="txt-title">Ứng dụng mô phỏng trò chơi đầy  thú vị</div>
					<div class="txt">Câu hỏi được lập trình như trò chơi, và bảng xếp hạng thi đua khiến cho bé hào hứng hơn khi làm bài</div>
				</div>

				<div class="slide-item">
					<img src="images/img-featured4.png" alt="Mô phỏng đề thi thật" class="img">
					<div class="txt-title">Mô phỏng đề thi thật</div>
					<div class="txt">Phần thi speaking các cấp độ. Thi viết chấm bằng AI</div>
				</div>

				<div class="slide-item">
					<img src="images/img-featured5.png" alt="Đánh giá trình độ và theo dõi sự tiến bộ qua mỗi bài thi" class="img">
					<div class="txt-title">Đánh giá trình độ và theo dõi sự tiến bộ qua mỗi bài thi</div>
					<div class="txt">Đáp án , báo cáo kết quả học tập, lưu lại kết quả và lịch sử làm bài</div>
				</div>
			</div>

			<button type="button" data-role="none" class="slick-prev slick-arrow" role="button"></button>
			<button type="button" data-role="none" class="slick-next slick-arrow" role="button"></button>
		</div>
	</div>
</div>
<!-- /.con-benefit -->

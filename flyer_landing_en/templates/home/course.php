<div class="con-course">
	<div class="container">
		<div class="box-couse">
			<h3 class="title-lg">CÁC GÓI HỌC - THI HẤP DẪN </h3>

			<div class="box-countdown mx-auto">
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="txt">
							Giảm giá đến 50% <br />
							Trong thời gian khuyến mãi
						</div>
					</div>

					<div class="col-12 col-md-6">
						<div id="countdown" class="countdown"></div>
					</div>
				</div>
			</div>

			<div class="box-pricing">
				<div class="row g-0">
					<div class="col-12 col-lg-3 col-free">
						<div class="box-free">
							<div class="title-col invisible d-none d-lg-block">Text</div>

							<div class="box-cost">
								<div class="txt-free">Miễn phí</div>
								<div class="price">0<span class="unit">đ</span></div>
							</div>

							<div class="box-btn">
								<a href="#" class="btn-blue btn-action">ĐĂNG NHẬP</a>
							</div>

							<div class="box-txt">
								<p class="mb-0">
									Truy cập giới hạn 1 số đề thi <br />
									KHÔNG GIỚI HẠN đề thi học kỳ
								</p>
							</div>
						</div>
					</div>

					<div class="col-12 col-lg-9">
						<div class="box-paid-course">
							<div class="title-col">CÁC GÓI TRẢ PHÍ</div>

							<div class="row row-cost">
								<div class="col-12 col-md-4">
									<div class="box-cost">
										<div class="txt-duration">1 năm</div>
										<div class="txt-recommend invisible d-none d-md-block">Text</div>
										<div class="price">490.000<span class="unit">đ</span></div>
										<div class="box-root-price">
											<span class="txt-root-price">Giá gốc</span>
											<span class="root-price">1.100.000</span>
											<span class="unit">đ</span>
										</div>
									</div>

									<div class="box-btn">
										<a href="#" class="btn-pink btn-action">ĐĂNG KÝ</a>
									</div>
								</div>

								<div class="col-12 col-md-4">
									<div class="box-cost">
										<div class="txt-duration">4 năm</div>
										<div class="txt-recommend">*GÓI ĐƯỢC YÊU THÍCH</div>
										<div class="price">1.190.000<span class="unit">đ</span></div>
										<div class="box-root-price">
											<span class="txt-root-price">Giá gốc</span>
											<span class="root-price">3.600.000</span>
											<span class="unit">đ</span>
										</div>
									</div>

									<div class="box-btn">
										<a href="#" class="btn-pink btn-action">ĐĂNG KÝ</a>
									</div>
								</div>

								<div class="col-12 col-md-4">
									<div class="box-cost">
										<div class="txt-duration">2 năm</div>
										<div class="txt-recommend invisible d-none d-md-block">Text</div>
										<div class="price">850.000<span class="unit">đ</span></div>
										<div class="box-root-price">
											<span class="txt-root-price">Giá gốc</span>
											<span class="root-price">1.700.000</span>
											<span class="unit">đ</span>
										</div>
									</div>

									<div class="box-btn">
										<a href="#" class="btn-pink btn-action">ĐĂNG KÝ</a>
									</div>
								</div>

							</div>

							<div class="box-txt">
								<p class="mb-0">
									Truy cập <strong>KHÔNG GIỚI HẠN kho 250+ đề thi</strong> chỉ với 1 tài khoản<br />
									Đề thi và tính năng mới <strong>tự động cập nhật liên tục</strong> trong suốt thời gian đăng ký<br />
									Tặng bộ <strong>tài liệu độc quyền miễn phí</strong> khi đăng ký tài khoản
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.box-couse -->
	</div>
	<div class="box-course-buy">
		<div class="container">
			<div class="box-title">
				<h4 class="title">Đăng ký mua tài khoản </h4>
				<div class="qr-code d-block d-md-none">
					<img src="images/img_code.png" alt="QR CODE">
				</div>
				<p>Quét mã MOMO hoặc chuyển khoản qua:</p>
			</div>
			<div class="row mb-1 info-bank">
				<div class="col-md-6">
					<p>Công ty cổ phần công nghệ flyer<br />
					Stk: 19036489566015 - techcombank chi nhánh hoàng đạo thuý
					</p>
				</div>
				<div class="col-md-6">
					<p>Nội dung: số đIện thoạI + tên con. <br />
						Flyer sẽ liên hệ & Gửi mã kích hoạt qua zalo 0367986532.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2 qr-code d-none d-md-block">
					<img src="images/img_code.png" alt="QR CODE">
				</div>
				<div class="col-md-10 box-form">
				<form class="form">
					<div class="row">
						<div class="col-md-6">
							<input type="email" class="form-control mb-2" id="email" placeholder="Email">
							<input type="phone" class="form-control mb-0" id="phone" placeholder="Số điện thoại liên lạc">
						</div>
						<div class="col-md-6">
							<input type="text" class="form-control mb-2" id="nameAge" placeholder="Tên học sinh - Độ tuổi">
							<button type="submit" class="btn btn-blue btn-submit">ĐĂNG KÝ NHẬN TƯ VẤN</button>
						</div>
					</div>

					
				
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.con-course -->

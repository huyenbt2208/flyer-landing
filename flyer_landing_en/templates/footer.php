<footer class="footer">
	<div class="container">
		<div class="box-inner">
			<div class="ft-logo">
				<div class="row">
					<div class="col-xs-12 col-md-3 ft-col col-logo">
						<div class="logo">
							<a href="homepage.php" class="over"><img src="images/logo-footer.png" alt=""></a>
						</div>
					</div>
				</div>
			</div>

			<div class="ft-main">
				<div class="row">
					<div class="col-xs-12 col-md-4 ft-col col-logo">
						<p class="txt mb-0">
							<strong>Công ty cổ phần công nghệ FLYER</strong><br />
							TAX ID: 0109322012<br />
							Address: N04 Hoang Dao Thuy, Cau Giay, Hanoi<br />
							Hotline 0979929295<br />
							Email: academic@flyer.Vn
						</p>
					</div>

					<div class="col-xs-12 col-md-2 ft-col col-about">
						<h3 class="title-footer">Company</h3>
						<nav class="nav">
							<ul class="list-style-none p-0 mb-0">
								<li><a href="#">About Us</a></li>
								<li><a href="#">Careers</a></li>
							</ul>
						</nav>
					</div>

					<div class="col-xs-12 col-md-2 ft-col">
						<h3 class="title-footer">Tearm of Use</h3>
						<nav class="nav">
							<ul class="list-style-none p-0 mb-0">
								<li><a href="#">About Us</a></li>
								<li><a href="#">Careers</a></li>
							</ul>
						</nav>
					</div>
					<div class="col-xs-12 col-md-4 ft-col">
						<h3 class="title-footer">Social network</h3>
						<nav class="nav">
							<ul class="list-style-none box-social p-0 mb-0">
								<li><a href="#"><img src="images/ic-facebook.png" alt=""></a></li>
								<li><a href="#"><img src="images/ic-twitter.png" alt=""></a></li>
								<li><a href="#"><img src="images/ic-instagram.png" alt=""></a></li>
								<li><a href="#"><img src="images/ic-youtube.png" alt=""></a></li>
								
							</ul>
						</nav>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- /.footer -->

<!-- JAVASCRIPT-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/js/bootstrap.min.js" integrity="sha512-OvBgP9A2JBgiRad/mM36mkzXSXaJE9BEIENnVEmeZdITvwT09xnxLtT4twkCa8m/loMbPHsvPl0T8lRGVBwjlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js" integrity="sha512-An4a3FEMyR5BbO9CRQQqgsBscxjM7uNNmccUSESNVtWn53EWx5B9oO7RVnPvPG6EcYcYPp0Gv3i/QQ4KUzB5WA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- <script src="js/slick.js"></script> -->
<script src="js/jquery.time-to.js"></script>
<script src="js/main.js"></script>

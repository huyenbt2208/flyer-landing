<header class="main-header">
	<div class="container">
		<div class="box-inner">
			<a href="#" class="logo d-none d-lg-block">
				<img src="images/logo.png" alt="FLYER">
			</a>

			<button type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasLeft" aria-controls="offcanvasLeft" class="btn-menu d-block d-lg-none">
				<svg width="18" height="12" viewBox="0 0 18 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0.94 0C0.420852 0 0 0.420852 0 0.94V1.06C0 1.57915 0.420853 2 0.940001 2H17.06C17.5791 2 18 1.57915 18 1.06V0.94C18 0.420852 17.5791 0 17.06 0H0.94ZM1 7C1.55228 7 2 6.55228 2 6C2 5.44772 1.55228 5 1 5C0.447715 5 0 5.44772 0 6C0 6.55228 0.447715 7 1 7ZM4.94 5C4.42085 5 4 5.42085 4 5.94V6.06C4 6.57915 4.42085 7 4.94 7H17.06C17.5791 7 18 6.57915 18 6.06V5.94C18 5.42085 17.5791 5 17.06 5H4.94ZM0 10.94C0 10.4209 0.420852 10 0.94 10H17.06C17.5791 10 18 10.4209 18 10.94V11.06C18 11.5791 17.5791 12 17.06 12H0.940001C0.420853 12 0 11.5791 0 11.06V10.94Z" fill="#ffffff"/></svg>
			</button>

			<div class="box-nav">
				<ul class="main-nav d-none d-lg-block">
					<li><a href="#"><span>Cuộc thi phá đảo</span></a></li>
					<li><a href="#"><span>Bí kíp luyện thi</span></a></li>
					<li><a href="#"><span>Bảng giá</span></a></li>
					<li><a href="#"><span>Tính năng mới</span></a></li>
					<li><a href="#"><span>Wanted</span></a></li>
				</ul>

				<div class="box-btns">
					<a href="#" class="btn-action btn-lightblue btn-login">Đăng nhập</a>
					<a href="#" class="btn-action btn-darkpink btn-try-free">Thi thử miễn phí</a>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- /.main-header -->

<div class="offcanvas offcanvas-start d-flex d-lg-none" tabindex="-1" id="offcanvasLeft" aria-labelledby="offcanvasLeftLabel">
	<button type="button" class="btn-close-offcanvas" data-bs-dismiss="offcanvas" aria-label="Close">
		<svg width="29" height="29" viewBox="0 0 29 29" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M27.6135 4.04337C28.4271 3.22977 28.4271 1.91068 27.6135 1.09709C26.7999 0.283497 25.4808 0.283496 24.6672 1.09709L14.3552 11.4091L4.04325 1.09709C3.22966 0.283497 1.91056 0.283498 1.09697 1.09709C0.283381 1.91068 0.283379 3.22977 1.09697 4.04337L11.4089 14.3553L1.09697 24.6673C0.283379 25.4809 0.283379 26.8 1.09697 27.6136C1.91056 28.4272 3.22966 28.4272 4.04325 27.6136L14.3552 17.3016L24.6672 27.6136C25.4808 28.4272 26.7999 28.4272 27.6135 27.6136C28.4271 26.8 28.4271 25.4809 27.6135 24.6673L17.3015 14.3553L27.6135 4.04337Z" fill="white"/></svg>
	</button>

  <div class="offcanvas-body">
		<div class="box-nav">
			<ul class="main-nav">
				<li><a href="#"><span>Cuộc thi phá đảo</span></a></li>
				<li><a href="#"><span>Bí kíp luyện thi</span></a></li>
				<li><a href="#"><span>Bảng giá</span></a></li>
				<li><a href="#"><span>Tính năng mới</span></a></li>
				<li><a href="#"><span>Wanted</span></a></li>
			</ul>
		</div>

		<div class="box-btns">
			<a href="#" class="btn-action btn-lightblue btn-login">Đăng nhập</a>
			<a href="#" class="btn-action btn-darkpink btn-try-free">Thi thử miễn phí</a>
		</div>

		<div class="box-logo">
			<a href="#" class="logo">
				<img src="images/logo.png" alt="FLYER">
			</a>
		</div>

  </div>
</div>
<!-- /.offcanvas -->

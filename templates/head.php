<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Landing</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- WEB FONTS -->
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">

		<!-- STYLESHEET -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" integrity="sha512-GQGU0fMMi238uA+a/bdWJfpUGKUkBdgfFdgBm72SUQ6BeyWjoY/ton0tEjH+OSH9iP4Dfh+7HM0I9f5eR0L/4w==" crossorigin="anonymous" referrerpolicy="no-referrer" />		
		<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" integrity="sha512-0OCvzZyRsIJ9Rg7O1S+Wf9ucukmVhyOg3h5eBG05aeH4NjG78fCEaEX3ofvndmukzENiX24fK5X/jPr6Y8yIdg==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->
		<link rel="stylesheet" type="text/css" href="css/slick/slick.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
		<!-- WARNING: Respond.js doesn't work if you view the page via file://-->
		<!--if lt IE 9
		script(src='https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')
		script(src='https://oss.maxcdn.com/respond/1.4.2/respond.min.js')
		-->
	</head>

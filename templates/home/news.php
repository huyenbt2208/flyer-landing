
<div class="con-news">
	<div class="container">
		<h3 class="title-lg"><span class="lg">BÁO CHÍ NÓI GÌ VỀ FLYER</span></h3>
		<div class="box-news">
			<div class="featured-news">
				<p class="featured"><img src="images/img_news.png" alt=""></p>
				<p class="img-partner"><img src="images/img_partner.png" alt=""></p>
			</div>
			<div class="box-news-info">
				<div class="box-item">
					<a class="lnk-title" href="">Báo điện tử Dân Trí <span>Phòng luyện thi ảo FLYER có gì đặc biệt?</span></a>
					<p>(Dân trí) - "Nghỉ hè rồi, cho bé Bơ học ở nhà hay đi học thêm tiếng Anh để không bị quên kiến thức nhỉ?" - mẹ Bơ nhăn trán nghĩ, ngay khi vừa nhận được thông báo nghỉ hè của cô chủ nhiệm. </p>
					<a class="box-lnk">Bấm để xem thêm</a>
				</div>
				<div class="box-item d-none d-sm-block">
					<a class="lnk-title" href="">vnexpress.net<span>Phương pháp luyện thi tiếng Anh lấy bằng Cambridge cho trẻ tiểu học</span></a>
					<p>Phòng luyện thi ảo Flyer với nhiều nội dung, thiết kế sinh động, bảng điểm đánh giá để học sinh tiểu học ôn tập lấy bằng Cambridge, TOEFL Primary. </p>
					<a class="box-lnk">Bấm để xem thêm</a>
				</div>
				
			</div>
		</div>
	</div>
</div>
<!-- /.con-faqs -->
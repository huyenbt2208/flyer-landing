<div class="con-planet">
	<div class="container">
		<div class="box-planet">
			<h3 class="title-md d-block d-sm-none">
				<span class="lg">LỘ TRÌNH CHUẨN </span>
				<span class="md">ĐƯỢC NGHIÊN CỨU BỞI CAMBRIDGE & TOEFL</span>
			</h3>
			<h4 class="d-block d-sm-none">LEVEL TIỂU HỌC</h4>
			<div class="planet-wrap">
				<div class="plan-top">
					<div class="box-item">
						<span class="txt-top">TOEFL PRIMARY</span>
						<p class="featured"><img src="images/img_planet8.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item ielts">
						<span class="txt-top">IELTS</span>
						<p class="featured"><img src="images/img_planet7.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item toefl-junior">
						<span class="txt-top">TOEFL JUNIOR</span>
						<p class="featured"><img src="images/img_planet6.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item pet">
						<span class="txt-top">PET</span>
						<p class="featured"><img src="images/img_planet5.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item ioe-top">
						<span class="txt-top">IOE</span>
						<p class="featured"><img src="images/img_planet4.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
				</div>
				<h3 class="title-md  d-none d-sm-block">
					<span class="lg">LỘ TRÌNH CHUẨN </span>
					<span class="md">ĐƯỢC NGHIÊN CỨU BỞI CAMBRIDGE & TOEFL</span>
				</h3>
				<h4 class="d-block d-sm-none">LEVEL TRUNG HỌC</h4>
				<div class="plan-bottom">
					<div class="box-item">
						<span class="txt-top">TOEFL PRIMARY</span>
						<p class="featured"><img src="images/img_planet5.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item ioe">
						<span class="txt-top">IOE</span>
						<p class="featured"><img src="images/img_planet4.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item starters active">
						<span class="txt-top">STARTERS</span>
						<p class="featured"><img src="images/img_planet1.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item movers">
						<span class="txt-top">MOVERS</span>
						<p class="featured"><img src="images/img_planet2.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
					<div class="box-item flyers">
						<span class="txt-top">FLYERS</span>
						<p class="featured"><img src="images/img_planet3.png" alt=""></p>
						<div class="box-txt"><p>Dành cho các bạn từ 12 tuổi</p></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<!-- /.con-planet -->
